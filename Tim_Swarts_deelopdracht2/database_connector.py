#!/usr/bin/python3

"""
This script connects to a database containg information
about students and exams. It uses the 'Tentamen' class made
in 'Tentamen.py' to store information, pulled from the database,
in objects. When run, it prints information about the database connection,
a list of names found in the 'studenten' table of the database
and, if given a specific name, all results of the given
student.
    Usage:
    python3.8 database_connector.py -u <username> -H <database_host> -d <database> -p <password> -n <student_name>
    For additional help information, run: python3.8 database_connector.py -h
Note: The example above uses python3.8, but any python version
above 3.7 should suffice (3.5 won't work because it doesn't allow f-strings)
"""

__author__ = 'Tim Swarts'
__version__ = 1.0

import sys
import argparse
import mysql.connector
from mysql.connector import errorcode
from Tentamen import Tentamen


class DatabaseConnection:
    """
    This class connects to a database and has several methods that can be used
    to extract information from the database. get_name_list creates a list of all
    names in the 'studenten' table, get_student_info creates as list of objects, one 'Tentamen'
    object for each exam made by a given student.
    """
    def __init__(self, args):
        """
        Here the information provided by the user is stored
            :param args: - All arguments parsed by a argparse parser, these will be used
                           to create the connection to the database.
        """
        # Set cnf
        if not args.username or not args.password or not args.database:
            # If not all login data is parsed through arg parse
            # We'll use a cnf file instead:
            print('Not all needed arguments were given, my.cnf file'
                  ' will be used for login instead')
            # Flip the cnf switch to true:
            self.cnf_check = True
        else:
            # When all login data is received, save it as self. values
            # and flip the cnf switch to false:
            self.cnf_check = False
            self.username = args.username
            self.password = args.password
            self.database = args.database
            self.host = args.host

        # Connect to database:
        self.connection = self.connect_database()

    def __str__(self):
        """
        String representation of an object made with this class
        """
        if not self.cnf_check:
            # If the cnf switch is set to false, we'll just return the login data via .format
            return "Username:\t{}\nHost:\t\t{}\nDatabase:\t{}".format(self.username,
                                                                      self.host,
                                                                      self.database)
        else:
            # If the cnf switch is set to true, we wil have to receive the login data
            # from the cnf file
            with open('my.cnf') as file:
                # So we loop through it and save everything except the first [client] line
                return ''.join(line for i, line in enumerate(file) if i > 0)

    def connect_database(self):
        """
        This method connects to the database. It will catch errors
        if they're found.
            :return cnx: Databases connection object.
        """
        try:
            # Try to connect to the database
            if self.cnf_check:
                # If the cnf switch is set to true, a cnf file will be used for all login data
                cnx = mysql.connector.connect(option_files='my.cnf')
            else:
                # Else, the argparse data will be used to try to connect
                cnx = mysql.connector.connect(user=self.username, password=self.password,
                                              database=self.database, host=self.host,
                                              raise_on_warnings=True)
            # Return the connection
            return cnx
        except mysql.connector.Error as err:
            # Give clear error messages:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something seems to be wrong with your username or password,\n"
                      "or there may be a problem with your my.cnf file")
                return False
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                print("The specified database does not seem to exist,\n"
                      "or there may be a problem with your my.cnf file")
                return False
            if err.errno == errorcode.CR_CONN_HOST_ERROR:
                print("You seem to have entered the wrong database host,\n"
                      "or there may be a problem with your my.cnf file")
                return False
            print(err)
            return False

    def get_name_list(self):
        """
        This method uses mysql syntax to fetch all student names.
            :return name_list: List with all student names
        """
        # Initiate query and execute it:
        query = "select naam from studenten;"
        cursor = self.connection.cursor(buffered=True)
        cursor.execute(query)
        # Fetch all records returned by the query:
        records = cursor.fetchall()
        # Put all values into a list:
        name_list = [''.join(name) for name in records]
        # Return the list:
        return name_list

    def get_student_info(self, name):
        """
        This method uses mysql syntax to fetch exam results of a single student
        given a student name. These results are then used to create a list of
        'Tentamen' objects.
            :param name: The name of a student. All results in the database
                         from this student, will be fetched.
            :return object_list: This is a list of 'Tentamen' objects.
        """
        # Initiate query that fetches all results from the given student:
        query = """select naam, cur_id, ex_datum, cijfer from studenten s join examens e
        on s.stud_id = e.stud_id where naam = %s;"""
        # Execute the query:
        cursor = self.connection.cursor(buffered=True)
        cursor.execute(query, (name,))
        # Fetch all records returned by the query:
        records = cursor.fetchall()
        if not records:
            # If the returned set is empty, report this to the user and leave the function:
            print('empty set returned for name = {}'.format(name))
            return False

        # Create a list of 'Tentamen' objects and return it:
        object_list = [Tentamen(record[0], record[1], record[2], record[3]) for record in records]
        return object_list


def argument_parser():
    """
    This functions is used to pass all arguments needed to run the script from the commandline
        return: parser.parse_args() - all the arguments given by the user
    """
    # Create a parser with a description of the function of the script:
    parser = argparse.ArgumentParser(description="This script creates a database connection\n"
                                                 "and has functions to extract information from\n"
                                                 "the database. If the script is run"
                                                 "without login data,\n The script will try"
                                                 "to pull login data from a my.cnf file")

    # Add arguments:
    parser.add_argument('-u', '--username', type=str, metavar='', required=False,
                        help='Your database username')
    parser.add_argument('-H', '--host', type=str, metavar='', required=False,
                        default='localhost', help='The database host')
    parser.add_argument('-d', '--database', type=str, metavar='', required=False,
                        help='The name of the database scheme')
    parser.add_argument('-p', '--password', type=str, metavar='', required=False,
                        help='Your database password,\ncorresponding to the given username')
    parser.add_argument('-n', '--name', type=str, metavar='', required=False,
                        default=None, help='A student name, this name wil be used\n'
                                           'to fetch all results of the student and return these'
                                           'as Tentamen objects')

    # Return user given variables:
    return parser.parse_args()


def main(args):
    """
    The main function creates a 'DatabaseConnection' object
    and calls its methods to print the desired outcome to the screen
    """
    # Create a database connection object:
    db_connection = DatabaseConnection(args)

    # Print the connection info:
    print(db_connection)

    if not db_connection.connection:
        # If connecting gave any errors, exit the function
        return 0

    # Print a list of names to the screen:
    names = ', '.join(db_connection.get_name_list())
    print(f'Student names: \t{names}')

    if args.name:
        # If a specific name is given, fetch all results of the student with this name:
        exams = db_connection.get_student_info(args.name)
        if exams:
            # If the results were fetched successfully (name didn't return an empty set)
            # print the results:
            for exam in exams:
                print(exam)
    return 0


if __name__ == '__main__':
    EXITCODE = main(argument_parser())
    sys.exit(EXITCODE)
