#!/usr/bin/python3

"""
This script defines a 'Tentamen' class that can be used when imported
into other scripts. This class contains information about a students
result of an exam. When run as a separate script, the user is required
to input this information themselves.
With this information an object instance is made of the class
and its values are printed to the screen.
    Usage: python3.8 Tentamen.py -s <student_name> -c <course_name> -d <date> -g <grade>
    For additional help information, run: python3.8 Tentamen.py -h
Note: The example above uses python3.8, but any python version
above 3.7 should suffice
"""

__author__ = 'Tim Swarts'
__version__ = 1.0

import sys
import argparse


class Tentamen:
    """
    This class contains information about a students
    result of an exam.
    """
    def __init__(self, student, vak, datum, cijfer):
        """
        Here the information provided by the user is stored
            :param student: The name of the student
            :param vak: The course of which the exam was taken
            :param datum: The data the exam was taken
            :param cijfer: The grade given to the student
        """
        # Set values:
        self.student = student
        self.vak = vak
        self.datum = datum
        self.cijfer = cijfer

    def __str__(self):
        """
        String representation of an object made with this class
        """
        # Return a nicely ordered string containing all information:
        return "Naam:\t{}\nVak:\t{}\nDatum:\t{}\nCijfer:\t{}\n".format(self.student,
                                                                       self.vak,
                                                                       self.datum,
                                                                       self.cijfer)


def argument_parser():
    """
    This functions creates an argument parser using the argparse library.
    This helps with error handling in case of missing arguments and provides
    a manual for the script when the script is run with an -h flag.
    """
    # Create a parser with a description of the function of the script
    parser = argparse.ArgumentParser(description="This script defines a 'Tentamen' class\n"
                                                 "containing information about an exam,\n"
                                                 "made by a student.")

    # Add arguments
    parser.add_argument('-s', '--student_name', type=str, metavar='', required=True,
                        help='The name of the student')
    parser.add_argument('-c', '--course', type=str, metavar='', required=True,
                        help='The name of the course')
    parser.add_argument('-d', '--date', type=str, metavar='', required=True,
                        help='The date the exam was taken')
    parser.add_argument('-g', '--grade', type=float, metavar='', required=True,
                        help='The grade the student was given')

    # Return user given variables
    return parser.parse_args()


def main(args):
    """
    This functions creates an instance of the 'Tentamen' class using arguments
    given with argparse and prints the values to the screen.
        :param args: All the argparse arguments given by the user.
        :return 0:
    """
    # Create and print new exam:
    exam = Tentamen(args.student_name, args.course, args.date, args.grade)
    print(exam)
    return 0


if __name__ == '__main__':
    # If the script is run, execute the main function:
    EXITCODE = main(argument_parser())
    sys.exit(EXITCODE)
