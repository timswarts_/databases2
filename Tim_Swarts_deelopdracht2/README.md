# Databases2
*please knit this file into a pdf for a more pleasant reading experience*
## Overview
Author:&ensp;&ensp;&ensp;&ensp;Tim Swarts  
Date:&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;December 2019  
Version:&ensp;&ensp;&ensp;1.0  

This directory contains 4 files (Tentamen.py, database_connector.py, my.cnf and this README):   
**Tentamen.py** defines a class 'Tentamens' that stores information about an exam.     
**database_connector** defines a class 'DatabaseConnection' that connects   
to a database, and a main function that creates an object of this class and calls its methods.    
**my.cnf** is an option file that can be used to login to a mysql database   
A more detailed explanation will follow shortly.  

## Requirements
* astroid==2.3.3
* isort==4.3.21
* lazy-object-proxy==1.4.3
* mccabe==0.6.1
* mysql-connector==2.2.9
* pylint==2.4.4
* six==1.13.0
* wrapt==1.11.2
* python==3.7 (or higher)

## Tentamen.py
This script defines a 'Tentamen' class that can be used when imported   
into other scripts. This class contains information about a students   
result of an exam. When run as a separate script, the user is required   
to input this information themselves.   
With this information an object instance is made of the class  
and its values are printed to the screen.     
&ensp;&ensp;Usage:   
&ensp;&ensp;```python3.8 Tentamen.py -s <student_name> -c <course_name> -d <date> -g <grade>```   
&ensp;&ensp;For additional help information, run:    
&ensp;&ensp;```python3.8 Tentamen.py -h```  
Note: The examples above use python3.8, but python3.7 should suffice

## database_connector.py
This script connects to a database containing information   
about students and exams. It uses the 'Tentamen' class made   
in 'Tentamen.py' to store information, pulled from the database,   
in objects. When run, it prints information about the database connection,   
a list of names found in the 'studenten' table of the database   
and, if given a specific name, all results of the given   
student.  
When not all needed login data is passed on when calling the script, the my.cnf file is used for   
connecting to the database instead.    
&ensp;&ensp;Usage:   
&ensp;&ensp;```python3.8 database_connector.py -u <username> -H <database_host> -d <database> -p <password> -n <student_name> ```  
&ensp;&ensp;For additional help information, run:  
&ensp;&ensp;```python3.8 database_connector.py -h```  
Note: The example above uses python3.8, but python3.7 should suffice

## my.cnf
This is an option file. MySQL can use this type of file as login input when connected to a database.    
Such a file should have the following structure:
```
[client]
user = <username>
password = <password>
host = <database_host>
database = <database>
```
The cnf file provided in this directory already has this structure, but you still have to   
personally change everything between arrow brackets ('<>') to your own login data.