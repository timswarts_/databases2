# If the tables already exist, drop them:
drop table if exists probes, microarrays, oligos, identifiers, genes, chromosomes, organisms;

# Create organisms table:
create table organisms(
    id         int              not null    auto_increment,
    genus      varchar(35)      not null,
    species    varchar(35)      not null,
    primary key (id)
);

# Create chromosome table:
create table chromosomes(
    id         int            not null    auto_increment,
    organism   int            not null,
    chromosome varchar(15)    not null,
    primary key (id),
    foreign key (organism) references organisms(id)
);

# Create gene table:
create table genes(
    id            int     not null    auto_increment,
    chromosome    int     not null,
    sequence      text    not null,
    primary key (id),
    foreign key (chromosome) references chromosomes(id)
);

# Create identifier table:
create table identifiers(
    id            int             not null    auto_increment,
    gene          int             not null,
    identifier    varchar(255)    not null,
    primary key (id),
    foreign key (gene) references genes(id)
);

# Create oligo table:
create table oligos(
    id                    int         not null    auto_increment,
    gene                  int         not null,
    sequence              char(25)    not null,
    gc_percentage         float       not null,
    melting_temp          float       not null,
    is_unique             bool        not null,
    contains_repeats      bool        not null,
    max2_dinuc_repeats    bool        not null,
    contains_hairpins     bool        not null,
    primary key (id),
    foreign key (gene) references genes(id)
);

# Create microarray table:
create table microarrays(
    id                 int    not null    auto_increment,
    quality            float    not null,
    incubation_temp    float    not null,
    primary key (id)
);

# Create probe table:
create table probes(
    id            int    not null    auto_increment,
    oligo         int    not null,
    microarray    int    not null,
    primary key (id),
    foreign key (oligo) references oligos(id),
    foreign key (microarray) references microarrays(id)
);