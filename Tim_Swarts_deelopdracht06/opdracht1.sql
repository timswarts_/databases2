# If the tables already exist, drop them:
drop table if exists probes, microarrays, oligos, identifiers, genes, chromosomes, organisms;

# Drop stored procedures:
drop procedure if exists sp_get_genes;
drop procedure if exists sp_get_tm_vs_oligos;
drop procedure if exists sp_mark_duplicate_oligos;
drop procedure if exists sp_get_oligos_by_tm;
drop procedure if exists sp_get_matrices_by_quality;
drop procedure if exists sp_create_probe;
drop procedure if exists sp_create_matrix;

# Create organisms table:
create table organisms(
    id         int              not null    auto_increment,
    genus      varchar(35)      not null,
    species    varchar(35)      not null,
    primary key (id)
);

# Create chromosome table:
create table chromosomes(
    id         int            not null    auto_increment,
    organism   int            not null,
    chromosome varchar(15)    not null,
    primary key (id),
    foreign key (organism) references organisms(id)
);

# Create gene table:
create table genes(
    id            int            not null    auto_increment,
    chromosome    int            not null,
    gene_name     varchar(30)    not null,
    primary key (id),
    foreign key (chromosome) references chromosomes(id)
);

# Create identifier table:
create table identifiers(
    id            int             not null    auto_increment,
    gene          int             not null,
    identifier    varchar(255)    not null,
    primary key (id),
    foreign key (gene) references genes(id)
);

# Create oligo table:
create table oligos(
    id                    int         not null    auto_increment,
    gene                  int         not null,
    sequence              char(25)    not null,
    gc_percentage         float       not null,
    melting_temp          float       not null,
    is_unique             bool        not null    default 0,
    contains_repeats      bool        not null    default 1,
    max2_dinuc_repeats    bool        not null    default 1,
    contains_hairpins     bool        not null    default 1,
    primary key (id),
    foreign key (gene) references genes(id)
);

# Create microarray table:
create table microarrays(
    id                 int    not null    auto_increment,
    quality            float,
    incubation_temp    float,
    primary key (id)
);

# Create probe table:
create table probes(
    id            int    not null    auto_increment,
    oligo         int    not null,
    microarray    int    not null,
    primary key (id),
    foreign key (oligo) references oligos(id),
    foreign key (microarray) references microarrays(id)
);

# This statement is for printing to screen that all tables have successfully been created:
select 'Table creation complete' as 'progress update:';

# Bulk imports follow, each time 1 line of the import file is ignored, because this is a header
# commas are used for separation of values or 'fields'

# Import the data from organisms.txt into the organisms table:
load data local infile 'data/organisms.txt'
    into table organisms
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from chromosomes.txt into the chromosomes table:
load data local infile 'data/chromosomes.txt'
    into table  chromosomes
    fields terminated by ','
    lines  terminated by '\n'
    ignore 1 lines;

# Import the data from genes.txt into the genes table:
load data local infile 'data/genes.txt'
    into table genes
    fields  terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from identifiers.txt into the identifiers table:
load data local infile 'data/identifiers.txt'
    into table identifiers
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from oligos.txt into the oligos table:
load data local infile 'data/oligos.txt'
    into table oligos
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from microarrays.txt into the microarrays table:
load data local infile 'data/microarrays.txt'
    into table microarrays
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from probes.txt into the probes table:
load data local infile  'data/probes.txt'
    into table probes
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# This statement is for printing to the screen that all data has been successfully loaded in:
select 'All data loaded in' as 'progress update:';

/**
Stored procedures
Before each stored procedure, the delimiter is set to '*' to prevent premature termination of the query.
This is needed since the procedure contains select queries that end with ';'.
After the definition is done, the delimiter can be set back to ';'. The following procedures are the
ones required for 'deelopdracht4'.
**/

# Create sp_get_genes procedure:
delimiter //
create procedure sp_get_genes()
begin
    # A select statement that fetches a gene and its external identifiers
    select gene_name, identifier from identifiers i join genes g on g.id = i.gene;
end //

# Create sp_get_tm_vs_oligos procedure:
create procedure sp_get_tm_vs_oligos()
begin
    # A select statement gives the amount of distinct melting_temps dived by the number of oligos in the oligos table
    select count(distinct(melting_temp)) / count(id) as
        'number of distinct melting temperature dived by amount of oligos'
         from oligos;
end //


# Create sp_mark_duplicate_oligos:
create procedure sp_mark_duplicate_oligos(in oligo_seq char(25))
    begin
        set @seq = oligo_seq;
        set @q = 'update oligos set is_unique = 1 where sequence = ?';
        prepare query from @q;
        execute query using @seq;
    end //

delimiter ;

# This statement is for printing to the screen that the first batch of stored procedures have been created:
select 'First procedures created' as 'progress update:';

# Create indices:
create fulltext index oligo_seq_idx
    on oligos (sequence);

create fulltext index gene_name_idx
    on genes (gene_name);

create index identifier_idx
    on identifiers (identifier);

create index quality_idx
    on microarrays (quality);

create index incubation_temp_idx
    on microarrays (incubation_temp);

# This statement is for printing to the screen that all indices have been made:
select 'Index creation complete' as 'progress update:';

# The following stored procedures are the ones required for 'deelopdracht6':
delimiter //
# Create sp_get_oligos_by_tm
create procedure  sp_get_oligos_by_tm(in min float, in max float)
    begin
        # Set variables:
        set @max = max;
        set @min = min;
        # Set query for filling the temporary table
        set @q = 'insert into oligo_ids select id from oligos where melting_temp between ? and ? order by melting_temp';

        /**
        Drop temporary table if it already exists
        This is necessary if we want to run the procedures multiple times in one session)
        **/
        drop table if exists oligo_ids;

        # Create temporary table to store ID's
        create temporary table oligo_ids (
            oligo_id    int
        );

        # Fill the table
        prepare query from @q;
        execute query using @min, @max;

        # Show the content of the temporary table
        select * from oligo_ids;
    end //

# Create sp_get_matrices_by_quality
create procedure sp_get_matrices_by_quality()
    begin
        select m.id, m.quality, m.incubation_temp,
        (select max(id) from genes) - count(distinct g.id) as 'genes_without_probe',
        count(distinct p.id) / count(distinct g.id) as 'avg_probes_per_gene'
        from genes g join oligos o on g.id = o.gene
        join probes p on o.id = p.oligo
        join microarrays m on p.microarray = m.id
        group by m.id order by genes_without_probe, avg_probes_per_gene desc;
    end //

# Create sp_create_probe
create procedure sp_create_probe(in matrix_id int, in oligo_id int)
    begin
        set @matrix_id = matrix_id;
        set @oligo_id = oligo_id;
        set @q = 'insert into probes(microarray, oligo) values(?, ?)';
        prepare query from @q;
        execute query using @matrix_id, @oligo_id;
    end //

# Create sp_create_matrix
create procedure sp_create_matrix(in melting_t float, in difference float)
    begin
        # Declare done
        declare  done int default false;
        # Declare variables needed for inserting into the probe table
        declare oligo int;
        declare array int;
        # Declare a cursor to fetch all oligo ids needed
        declare prob_cur cursor for select * from oligo_ids;
        # Declare continue handler
        declare continue handler for not found set done = 1;

        # Set variables needed when calling get_oligos
        set @temp = melting_t;
        set @diff = difference;
        set @min = @temp - @diff;
        set @max = @temp + @diff;

        # Create a new microarray
        insert into microarrays(quality, incubation_temp) values (NULL, NULL);
        # Store its id in the array variable declared previously
        select last_insert_id() into array;
        # Get all oligos, this creates the temporary table used in the cursor
        call sp_get_oligos_by_tm(@min, @max);

        # Loop through the temporary oligo_ids table
        open prob_cur;
        read_loop: loop
            # Fetch each oligo id into the oligo variable declared previously
            fetch  prob_cur into oligo;
            # Insert new probes into the probe table, using the oligo and array variable
            insert into probes(oligo, microarray) values (oligo, array);

            # End the loop:
            if done then
                leave read_loop;
            end if;
        end loop;
        close prob_cur;
        # End
    end //
delimiter ;

# This statement is for printing to the screen that the second batch of stored procedures have been created:
select 'Last procedures created' as 'progress update:';
