# If the tables already exist, drop them:
drop table if exists probes, microarrays, oligos, identifiers, genes, chromosomes, organisms;

# Drop stored procedures:
drop procedure if exists sp_get_genes;
drop procedure if exists sp_get_tm_vs_oligos;
drop procedure if exists sp_mark_duplicate_oligos;


# Create organisms table:
create table organisms(
    id         int              not null    auto_increment,
    genus      varchar(35)      not null,
    species    varchar(35)      not null,
    primary key (id)
);

# Create chromosome table:
create table chromosomes(
    id         int            not null    auto_increment,
    organism   int            not null,
    chromosome varchar(15)    not null,
    primary key (id),
    foreign key (organism) references organisms(id)
);

# Create gene table:
create table genes(
    id            int            not null    auto_increment,
    chromosome    int            not null,
    gene_name     varchar(30)    not null,
    primary key (id),
    foreign key (chromosome) references chromosomes(id)
);

# Create identifier table:
create table identifiers(
    id            int             not null    auto_increment,
    gene          int             not null,
    identifier    varchar(255)    not null,
    primary key (id),
    foreign key (gene) references genes(id)
);

# Create oligo table:
create table oligos(
    id                    int         not null    auto_increment,
    gene                  int         not null,
    sequence              char(25)    not null,
    gc_percentage         float       not null,
    melting_temp          float       not null,
    is_unique             bool        not null    default 0,
    contains_repeats      bool        not null    default 1,
    max2_dinuc_repeats    bool        not null    default 1,
    contains_hairpins     bool        not null    default 1,
    primary key (id),
    foreign key (gene) references genes(id)
);

# Create microarray table:
create table microarrays(
    id                 int    not null    auto_increment,
    quality            float    not null,
    incubation_temp    float    not null,
    primary key (id)
);

# Create probe table:
create table probes(
    id            int    not null    auto_increment,
    oligo         int    not null,
    microarray    int    not null,
    primary key (id),
    foreign key (oligo) references oligos(id),
    foreign key (microarray) references microarrays(id)
);

# Bulk imports follow, each time 1 line of the import file is ignored, because this is a header
# commas are used for separation of values or 'fields'

# Import the data from organisms.txt into the organisms table:
load data local infile 'data/organisms.txt'
    into table organisms
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from chromosomes.txt into the chromosomes table:
load data local infile 'data/chromosomes.txt'
    into table  chromosomes
    fields terminated by ','
    lines  terminated by '\n'
    ignore 1 lines;

# Import the data from genes.txt into the genes table:
load data local infile 'data/genes.txt'
    into table genes
    fields  terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from identifiers.txt into the identifiers table:
load data local infile 'data/identifiers.txt'
    into table identifiers
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

# Import the data from oligos.txt into the oligos table:
load data local infile 'data/oligos.txt'
    into table oligos
    fields terminated by ','
    lines terminated by '\n'
    ignore 1 lines;

/**
Stored procedures
Before each stored procedure, the delimiter is set to '*' to prevent premature termination of the query.
This is needed since the procedure contains select queries that end with ';'.
After the definition is done, the delimiter can be set back to ';'.
**/

# Create sp_get_genes procedure:
delimiter *
create procedure sp_get_genes()
begin
    # A select statement that fetches a gene and its external identifiers
    select gene_name, identifier from identifiers i join genes g on g.id = i.gene;
end *

# Create sp_get_tm_vs_oligos procedure:
create procedure sp_get_tm_vs_oligos()
begin
    # A select statement gives the amount of distinct melting_temps dived by the number of oligos in the oligos table
    select count(distinct(melting_temp)) / count(id) as
        'number of distinct melting temperature dived by amount of oligos'
         from oligos;
end *


# Create sp_mark_duplicate_oligos
create procedure sp_mark_duplicate_oligos(in oligo_seq char(25))
    begin
    set @seq = oligo_seq;
        update oligos set is_unique = 1 where sequence = @seq;
    end *

delimiter ;

# Create indices:
create fulltext index oligo_seq_idx
    on oligos (sequence);

create fulltext index gene_name_idx
    on genes (gene_name);

create unique index identifier_idx
    on identifiers (identifier);

create index quality_idx
    on microarrays (quality);

create index incubation_temp_idx
    on microarrays (incubation_temp);