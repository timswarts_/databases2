# Indices Kiezen
## Algemeen

Indices zijn speciale opzoektabellen die de database-zoekmachine kan gebruiken om het ophalen van gegevens   
te versnellen. Simpel gezegd, een index is een verwijzing naar gegevens in een tabel.

We kennen 3 soorten indices: **spatial**, **fulltext** en **unique**.   

**Spatial** indices worden gebruikt voor geometrie of geo-locaties. Deze soort indices is voor mijn database  
irrelevant, omdat de database geen dergelijke data bevat.  
**Fulltext** indices zijn geschikt voor het zoeken naar patronen in grotere stukken tekst,    
bijvoorbeeld met behulp van reguliere expressies.   
**Unique** indices kunnen gebruikt worden om het zoeken naar specifieke unieke waarden te versnellen.    
Mijn database bevat naast de primary keys weinig werkelijk unieke kolommen. De primary keys zijn automatisch   
oplopende gehele getallen en hebben geen aparte indices nodig, vandaar dat ook deze vorm van indices   
niet erg relevant is.

## Per tabel
### Organisms
De organismen tabel is een relatief kleine tabel, waar niet veel data verwacht wordt. Dit wil zeggen dat de tabel    
ingevuld niet veel rijen zal bevatten. Het is uiteraard mogelijk om data van een grote hoeveelheid verschillende   
organismen op te slaan, maar dit zal in de praktijk niet snel gebeuren.  
Het is daarom niet nodig om indices aan de tabel toe te voegen.

### Chromosomes
De chromosomen tabel is groter dan de organismen tabel, want een organisme bevat meerdere chromosomen.   
Echter is de tabel nog niet zo groot dat indices nodig of behulpzaam zijn. Een organisme heeft namelijk maar een   
gelimiteerde hoeveelheid chromosomen. Bij het opslaan van gegevens over meerdere organismen groeit deze tabel wel    
aanzienlijk in grote, maar niet dusdanig veel dat indices op hun plaats zijn.

### Genes
De genen tabel is wel een tabel die indexing kan gebruiken.    
Een organisme heeft meerdere chromosomen en elk chromosoom bevat een grote hoeveelheid genen.   
Het is misschien wenselijk om te zoeken op een specifieke gen naam of een aantal gen namen die op elkaar lijken.   
Het is dan handig om een fulltext index toe te voegen. Die index wordt als volgt gemaakt:   
```mysql
create fulltext index gene_name_idx
    on genes (gene_name);
```

### Identifiers
De identifiers tabel bevat unieke identifiers voor elk gen. Een unique index is hier dus op zijn plaats:
```mysql
create fulltext index gene_name_idx
    on genes (gene_name);
```

### Oligos
De oligo tabel zal hoogst waarschijnlijk zeer veel rijen bevatten, omdat elk gen meerdere oligos kan hebben.  
Er wordt een fulltext index toegevoegd om het zoeken op specifieke sequenties binnen de tabel te versnellen:  
```mysql
create fulltext index oligo_seq_idx
    on oligos (sequence);
```

### Microarrays
Bij de microarray tabel worden tevens veel rijen aan data verwacht. Het kan handig zijn om in deze tabel snel te kunnen   
zoeken naar bepaalde kwaliteitswaarden, of bepaalde incubatietemperaturen, daarom wordt voor beide een index toegevoegt.  
In dit geval voegt het niets toe om er een fulltext index van te maken, het gaat om specifieke waarden. Deze waarden
zijn tevens niet uniek, daarom wordt er geen type gespecificeerd:
```mysql
create index quality_idx
    on microarrays (quality);

create index incubation_temp_idx
    on microarrays (incubation_temp);
```

### Probes
De probes tabel is de tabel waar je waarschijnlijk de meeste rijen aan data kan verwachten, omdat elke microarray   
duizenden probes kan bevatten. Deze tabel heeft echter geen kolommen waarvan ik verwacht dat er veel op gezocht wordt,   
daarom worden er geen indices toegevoegd.