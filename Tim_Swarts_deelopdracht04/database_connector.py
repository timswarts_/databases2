#!/usr/bin/python3

"""
This script contains a class that connects to a database and
executes stored procedures on the database
usage: python3 database_connector [-o <oligo_sequence>]
"""

__author__ = 'Tim Swarts'
__version__ = '1.0'

import sys
import argparse
import mysql.connector
from mysql.connector import errorcode


def connect_database():
    """
    This method connects to the database. It will catch errors
    if they're found.
        :return cnx: Databases connection object.
    """
    try:
        # Try to connect to the database
        # If the cnf switch is set to true, a cnf file will be used for all login data
        cnx = mysql.connector.connect(option_files='my.cnf')

        # Return the connection
        return cnx

    except mysql.connector.Error as err:
        # Give clear error messages:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something seems to be wrong with your username or password,\n"
                  "or there may be a problem with your my.cnf file")
            return False
        if err.errno == errorcode.ER_BAD_DB_ERROR:
            print("The specified database does not seem to exist,\n"
                  "or there may be a problem with your my.cnf file")
            return False
        if err.errno == errorcode.CR_CONN_HOST_ERROR:
            print("You seem to have entered the wrong database host,\n"
                  "or there may be a problem with your my.cnf file")
            return False
        print(err)
        return False


class DatabaseConnection:
    """
    Class that connects to database and has methods to call stored procedures
    """
    def __init__(self, oligo='1'):
        """
        setting required class variables
        """
        self.connection = connect_database()
        self.oligo = oligo

    def __str__(self):
        """
        String representation of an object made with this class
        :return: String
        """

        with open('my.cnf') as file:
            return ''.join(line for i, line in enumerate(file) if i > 0)

    def sp_get_genes(self):
        """
        Execute sp_get_genes procedure
        """
        # Set cursor
        cursor = self.connection.cursor()
        # Call procedure
        cursor.callproc('sp_get_genes')

        # Fetch results
        for result in cursor.stored_results():
            fetched = result.fetchall()
            # Parse results into nice format
            rows = [', '.join(row) for row in fetched]
            records = '\n'.join(row for row in rows)
            # Print the results:
            print(f'genes and identifiers:\n{records}')

    def sp_get_tm_vs_oligos(self):
        """
        Execute sp_get_tm_vs_oligos procedure
        """
        cursor = self.connection.cursor()
        cursor.callproc('sp_get_tm_vs_oligos')
        for result in cursor.stored_results():
            print(f'value: {result.fetchall()[0][0]}')

    def sp_mark_duplicate_oligos(self):
        """
        Execute sp_mark_duplicate_oligos
        """
        cursor = self.connection.cursor()
        args = (self.oligo,)
        cursor.callproc('sp_mark_duplicate_oligos', args)
        print('oligo successfully marked as unique')
        self.connection.commit()


def argument_parser():
    """
    This functions is used to pass all arguments needed to run the script from the commandline
        return: parser.parse_args() - all the arguments given by the user
    """
    # Create a parser with a description of the function of the script:
    parser = argparse.ArgumentParser(description="")

    # Add argument:
    parser.add_argument('-o', '--oligo', type=str, metavar='', required=False,
                        help='oligo from which to change is_unique argument')

    # Return user given variables:
    return parser.parse_args()


def main(args):
    """
    main function that creates class instance and calls methods
    """
    if args.oligo:
        database = DatabaseConnection(args.oligo)
        if not database.connection:
            return 1
        print('get genes:')
        database.sp_get_genes()
        print('get distinct melting temperatures devided by number of oligos:')
        database.sp_get_tm_vs_oligos()
        print('oligo specified, oligo will be updated')
        database.sp_mark_duplicate_oligos()

    else:
        database = DatabaseConnection()
        if not database.connection:
            return 1
        print('get genes:')
        database.sp_get_genes()
        print('get distinct melting temperatures devided by number of oligos:')
        database.sp_get_tm_vs_oligos()
        print('no oligo specified, no oligo updated')
    return 0


if __name__ == '__main__':
    EXITCODE = main(argument_parser())
    sys.exit(EXITCODE)
