# Overwiev
Author: Tim   
Date: 19/12/2019

# Requirements
astroid==2.3.3   
isort==4.3.21    
lazy-object-proxy==1.4.3  
mccabe==0.6.1   
mysql-connector==2.2.9  
pylint==2.4.4  
six==1.13.0   
wrapt==1.11.2  

# opdracht1.sql
Run this sql file to set up the data base
It can be run as follows:   
mysql < opdracht1.sql

# database_connector.py
Connects to database and runs all stored procedures   
usage: python3 database_connector [-o <oligo_sequence>]

# my.cnf
This is an option file. MySQL can use this type of file as login input when connected to a database.    
Such a file should have the following structure:
```
[client]
user = <username>
password = <password>
host = <database_host>
database = <database>
```
The cnf file provided in this directory already has this structure, but you still have to   
personally change everything between arrow brackets ('<>') to your own login data.

# data
This is a map of bulk import files that will be used to fill the database.